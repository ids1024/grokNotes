package com.grok.notes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.google.common.base.Optional;
import com.grok.notes.document.StorageAccessFramework;
import com.grok.notes.util.Style;

public class GrokNotesActivity extends AppCompatActivity
        implements HomeButton.ActivityWithHomeButton {
    private NavDrawer nav;
    private Optional<? extends Runnable> backButtonListener = Optional.absent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Style.applyTo(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grok_notes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        nav = NavDrawer.create(this, toolbar);
        if (Preference.KEEP_SCREEN_ON.getBoolean(this, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onResume() {
        if (GrokNotes.getInstance().isFirstRun()) {
            WelcomeFragment.show(this);
            GrokNotes.getInstance().setRunned();
        }
        if (getIntent().getBooleanExtra("openToWelcome", false)) {
            getIntent().removeExtra("openToWelcome");
            WelcomeFragment.show(this);
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (backButtonListener.isPresent()) {
            backButtonListener.get().run();
            backButtonListener = Optional.absent();
        } else if (getFragmentManager().getBackStackEntryCount() == 0) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        } else {
            getFragmentManager().popBackStack();
            if (getFragmentManager().getBackStackEntryCount() == 1) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                nav.setDrawerIndicatorEnabled(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.grok_notes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (StorageAccessFramework.canHandle(requestCode)) {
            StorageAccessFramework.handleResult(this, requestCode, resultCode, resultData);
        } else {
            throw new IllegalArgumentException("Intent result unrecognized with code " + requestCode);
        }
    }

    @Override
    public NavDrawer getNav() {
        return nav;
    }

    @Override
    public void setBackButtonListener(Optional<? extends Runnable> backButtonListener) {
        this.backButtonListener = backButtonListener;
    }
}

package com.grok.notes;

import android.app.Activity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.google.common.base.Optional;

public class HomeButton {
    public static void openDrawer(Activity context) {
        ((DrawerLayout) context.findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
    }

    public static HomeButton of(Activity context) {
        ActivityWithHomeButton support = (ActivityWithHomeButton) context;
        return new HomeButton(
                support,
                support.getNav(),
                ((AppCompatActivity) support).getSupportActionBar(),
                (DrawerLayout) context.findViewById(R.id.drawer_layout));
    }

    private final ActivityWithHomeButton parent;
    private final NavDrawer nav;
    private final ActionBar bar;
    private final DrawerLayout drawer;

    public HomeButton(ActivityWithHomeButton parent, NavDrawer nav, ActionBar bar, DrawerLayout drawer) {
        this.parent = parent;
        this.nav = nav;
        this.bar = bar;
        this.drawer = drawer;
    }

    public void setAsUpArrow(final Listener onUpClicked) {
        nav.setDrawerIndicatorEnabled(false);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        parent.setBackButtonListener(Optional.of(new Runnable() {
            @Override
            public void run() {
                onUpClicked.onHomeButtonPressed(HomeButton.this);
                setAsDrawer();
            }
        }));
    }

    public void setAsBackButton() {
        nav.setDrawerIndicatorEnabled(false);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        parent.setBackButtonListener(Optional.absent());
    }

    public void setAsDrawer() {
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(false);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        nav.setDrawerIndicatorEnabled(true);
        parent.setBackButtonListener(Optional.absent());
    }

    public interface ActivityWithHomeButton {
        void setBackButtonListener(Optional<? extends Runnable> runnable);

        NavDrawer getNav();
    }

    public interface Listener {
        void onHomeButtonPressed(HomeButton button);
    }
}

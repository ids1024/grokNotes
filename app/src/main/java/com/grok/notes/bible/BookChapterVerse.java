package com.grok.notes.bible;

import com.google.common.base.CaseFormat;
import com.google.common.base.Optional;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookChapterVerse {
    public static Optional<BookChapterVerse> fromBibleSiteUrl(String url) {
        String rawRef = url.substring(url.lastIndexOf('/') + 1).replace('.', ' ');
        Matcher m = Pattern.compile("(.*)\\s(\\d*)\\s([\\w-]*)\\s(.*)").matcher(rawRef);
        m.find();
        if (m.groupCount() > 0) {
            String book = CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, m.group(1));
            Integer chapter = Integer.parseInt(m.group(2));
            String verses = m.group(3);
            String translation = m.group(4).toUpperCase();
            return Optional.of(new BookChapterVerse(book, chapter, verses, translation));
        } else {
            return Optional.absent();
        }
    }

    public static Optional<BookChapterVerse> parse(String typicalFormat) {
        // (\d*.*\d*) is because there are non-printing chars in the string
        Matcher m = Pattern.compile("(.*)\\s(\\d*):(\\d*.*\\d*)\\s(.*)").matcher(typicalFormat);
        m.find();
        if (m.groupCount() > 0) {
            String book = CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, m.group(1));
            Integer chapter = Integer.parseInt(m.group(2));
            String verses = m.group(3);
            String translation = m.group(4).toUpperCase();
            return Optional.of(new BookChapterVerse(book, chapter, verses, translation));
        } else {
            return Optional.absent();
        }
    }

    private final String book;
    private final int chapter;
    private final String verses;
    private final String translation;

    public BookChapterVerse(String book, int chapter, String verses, String translation) {
        this.book = book;
        this.chapter = chapter;
        this.verses = verses;
        this.translation = translation;
    }

    @Override
    public String toString() {
        return String.format("%s %s:%s (%s)", book, chapter, verses, translation);
    }
}

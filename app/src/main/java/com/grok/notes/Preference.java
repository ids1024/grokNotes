package com.grok.notes;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.common.base.Optional;
import com.google.gson.Gson;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public enum Preference {
    // Find the formatting bar items in {@link FormattingBarItem}

    KEEP_SCREEN_ON("keep_screen_on"),
    SAF_OPENS_ANY_TYPE("saf_opens_any_type"),
    OPEN_PREVIEW_BY_DEFAULT("open_preview_first"),
    AUTO_SAVE_INTERVAL_SECONDS("auto_save_interval"),

    THEME("theme"),
    LIGHT_THEME_PRIMARY_COLOR("light_theme_primary_color"),
    LIGHT_THEME_ACCENT_COLOR("light_theme_accent_color"),

    EDITOR_FONT("editor_font"),
    PREVIEW_FONT("preview_font"),
    EDITOR_TEXT_SIZE("editor_text_size"),
    PREVIEW_TEXT_SIZE("preview_text_size"),;

    public static final int TAB_LENGTH = 4;

    public final String key;

    Preference(String key) {
        this.key = key;
    }

    public String getString(Context c, String secondChoice) {
        return getDefaultSharedPreferences(c).getString(key, secondChoice);
    }

    public int getInt(Context c, int secondChoice) {
        SharedPreferences prefs = getDefaultSharedPreferences(c);
        try {
            return prefs.getInt(key, secondChoice);
        } catch (ClassCastException maybeAString) {
            return Integer.parseInt(prefs.getString(key, "" + secondChoice));
        }
    }

    public boolean getBoolean(Context c, boolean secondChoice) {
        return getDefaultSharedPreferences(c).getBoolean(key, secondChoice);
    }

    public static void put(Activity context, String key, Object toCommit) {
        put(getDefaultSharedPreferences(context), key, toCommit);
    }

    public static void put(SharedPreferences destination, String key, Object toCommit) {
        SharedPreferences.Editor prefsEditor = destination.edit();
        Gson gson = new Gson();
        prefsEditor.putString(key, gson.toJson(toCommit));
        prefsEditor.commit();
    }

    public static <T> Optional<T> get(Activity context, String key, Class<T> helper) {
        return get(getDefaultSharedPreferences(context), key, helper);
    }

    public static <T> Optional<T> get(SharedPreferences source, String key, Class<T> helper) {
        Gson gson = new Gson();
        String json = source.getString(key, null);
        return json == null ? Optional.absent() : Optional.of(gson.fromJson(json, helper));
    }
}

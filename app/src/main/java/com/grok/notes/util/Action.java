package com.grok.notes.util;

public interface Action {
    void run();
}

package com.grok.notes.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Layout;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.common.base.MoreObjects;

public class ScrollPosition implements Parcelable {
    private final int lineOffset;
    private final int percentageBetweenLineAndPreviousLine;
    private final int scrollY;

    public static ScrollPosition top() {
        return new ScrollPosition(0, 0, 0);
    }

    public static ScrollPosition from(ScrollView s, TextView t) {
        int y = s.getScrollY();
        Layout layout = t.getLayout();
        int topPadding = -layout.getTopPadding();
        if (y <= topPadding) {
            float calculated = (float) (topPadding - y) / t.getLineHeight();
            return new ScrollPosition((int) calculated, (int) (calculated - ((int) calculated)), y);
        }
        int line = layout.getLineForVertical(y - 1) + 1;
        int offset = layout.getLineStart(line);
        int above = layout.getLineTop(line) - y;
        return new ScrollPosition(offset, above / t.getLineHeight(), y);

    }

    private ScrollPosition(int lineOffset, int percentageBetweenLineAndPreviousLine, int scrollY) {
        this.lineOffset = lineOffset;
        this.percentageBetweenLineAndPreviousLine = percentageBetweenLineAndPreviousLine;
        this.scrollY = scrollY;
    }

    public void applyTo(final ScrollView s, final TextView t) {
        s.post(() -> scrollTo(s, t));
    }

    public void applyTo(final WebView w) {
        w.post(() -> w.scrollTo(0, scrollY));
    }

    private void scrollTo(ScrollView s, TextView t) {
        int above = percentageBetweenLineAndPreviousLine * t.getLineHeight();
        Layout layout = t.getLayout();
        int line = layout.getLineForOffset(lineOffset);
        int y = (line == 0 ? -layout.getTopPadding() : layout.getLineTop(line)) - above;
        s.scrollTo(0, y);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("lineOffset", lineOffset)
                .add("percentageBetweenLineAndPrevious", percentageBetweenLineAndPreviousLine)
                .add("scrollY", scrollY)
                .toString();
    }

    public static final Creator<ScrollPosition> CREATOR = new Creator<ScrollPosition>() {
        @Override
        public ScrollPosition createFromParcel(Parcel in) {
            return new ScrollPosition(in);
        }

        @Override
        public ScrollPosition[] newArray(int size) {
            return new ScrollPosition[size];
        }
    };

    protected ScrollPosition(Parcel in) {
        lineOffset = in.readInt();
        percentageBetweenLineAndPreviousLine = in.readInt();
        scrollY = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(lineOffset);
        dest.writeInt(percentageBetweenLineAndPreviousLine);
        dest.writeInt(scrollY);
    }
}

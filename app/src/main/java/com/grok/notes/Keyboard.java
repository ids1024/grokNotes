package com.grok.notes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class Keyboard {
    private static final int DefaultKeyboardDP = 100;
    private static final int EstimatedKeyboardDP = DefaultKeyboardDP +
            (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
    private static final Rect r = new Rect();

    public static ViewTreeObserver.OnGlobalLayoutListener add(
            Activity context, final OnKeyboardVisibilityListener listener) {
        // Source: http://stackoverflow.com/a/18992807/608347
        ViewTreeObserver.OnGlobalLayoutListener isKeyboardOpen = new ViewTreeObserver.OnGlobalLayoutListener() {
            private boolean wasOpened;

            @Override
            public void onGlobalLayout() {
                boolean isShown = isVisible(context);
                if (isShown == wasOpened) {
                    return;
                }
                wasOpened = isShown;
                listener.onVisibilityChanged(isShown);
            }
        };
        View activityRootView = ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(isKeyboardOpen);
        return isKeyboardOpen;
    }

    public static boolean isVisible(Activity context) {
        View activityRootView = ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
        // Convert the dp to pixels.
        int estimatedKeyboardHeight = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                EstimatedKeyboardDP,
                activityRootView.getResources().getDisplayMetrics());

        // Conclude whether the keyboard is shown or not.
        activityRootView.getWindowVisibleDisplayFrame(r);
        int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);
        return heightDiff >= estimatedKeyboardHeight;
    }

    public static void close(Activity context, EditText input) {
        if (input != null) {
            input.clearFocus();
        }
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static ViewTreeObserver.OnGlobalLayoutListener hideFabWhenOpen(final Activity a) {
        return add(a, new Keyboard.OnKeyboardVisibilityListener() {
            @Override
            public void onVisibilityChanged(boolean visible) {
                View fab = a.findViewById(R.id.fab);
                if (fab != null && visible) {
                    fab.setVisibility(View.GONE);
                } else {
                    fab.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public static void ignoreFabVisibilityListener(
            Activity context, ViewTreeObserver.OnGlobalLayoutListener listener) {
        View activityRootView = ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
        activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
    }

    public interface OnKeyboardVisibilityListener {
        void onVisibilityChanged(boolean visible);
    }
}

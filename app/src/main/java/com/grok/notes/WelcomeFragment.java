package com.grok.notes;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class WelcomeFragment extends Fragment {
    public static DialogInterface.OnClickListener showFromDialog(final Activity context) {
        return (dialog, which) -> show(context);
    }

    public static void show(Activity context) {
        context.getFragmentManager().beginTransaction()
                .replace(R.id.welcome, new WelcomeFragment())
                .commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_splash, container, false);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getResources().getString(R.string.app_name));
        HomeButton.of(getActivity()).setAsDrawer();
        HomeButton.openDrawer(getActivity());
    }
}

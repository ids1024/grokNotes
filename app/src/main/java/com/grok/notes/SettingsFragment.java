package com.grok.notes;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.grok.notes.edit.Divider;
import com.grok.notes.util.Style;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private Divider previousState;
    private int fabVisiblity = View.GONE;
    private boolean reloadLeftContainer = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        addPreferencesFromResource(R.xml.preferences);
        PreferenceScreen screen = getPreferenceScreen();

        PreferenceCategory category = new PreferenceCategory(screen.getContext());
        category.setTitle("Enabled formatting buttons");

        PreferenceScreen secondScreen = getPreferenceManager().createPreferenceScreen(screen.getContext());
        secondScreen.setTitle("Formatting bar");
        secondScreen.setSummary("Change formatting bar items");
        secondScreen.addPreference(category);

        screen.addPreference(secondScreen);

        for (FormattingBarItem item : FormattingBarItem.values()) {
            SwitchPreference p = new SwitchPreference(getActivity());
            p.setKey(item.key);
            p.setChecked(item.enabledByDefault);
            p.setDefaultValue(item.enabledByDefault);
            p.setSummary(item.summary);
            p.setTitle(item.title);
            category.addPreference(p);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        previousState = Divider.saveState(getActivity());
        Divider.hideIfPresent(getActivity());
        v.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        View fab = getActivity().findViewById(R.id.fab);
        fabVisiblity = fab.getVisibility();
        fab.setVisibility(View.GONE);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.grok_notes, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeButton.of(getActivity()).setAsBackButton();
        getActivity().invalidateOptionsMenu();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        if (reloadLeftContainer) {
            reloadLeftContainer();
        }
        HomeButton.of(getActivity()).setAsDrawer();
        previousState.restore(getActivity());
        getActivity().findViewById(R.id.fab).setVisibility(fabVisiblity);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (Preference.KEEP_SCREEN_ON.key.equals(key)) {
            if (sharedPreferences.getBoolean(Preference.KEEP_SCREEN_ON.key, false)) {
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } else {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        } else if (key.startsWith("fb_")) {
            // formatting bar preference changed, reload them.  If no edit view then reload is automatic
            View maybe = getActivity().findViewById(R.id.edit);
            if (maybe != null) {
                FormattingBar.enable(getActivity()).applyTo((EditText) maybe).hide();
            }
        } else if (Preference.EDITOR_FONT.key.equals(key)
                || Preference.EDITOR_TEXT_SIZE.key.equals(key)) {
            if (getActivity().findViewById(R.id.edit) != null) {
                reloadLeftContainer = true;
                // todo - keyboard opens randomly here?
            }
        } else if (Preference.PREVIEW_FONT.key.equals(key)
                || Preference.PREVIEW_TEXT_SIZE.key.equals(key)) {
            if (getActivity().findViewById(R.id.previewContent) != null) {
                reloadLeftContainer = true;
            }
        } else if (Preference.THEME.key.equals(key)
                || Preference.LIGHT_THEME_PRIMARY_COLOR.key.equals(key)
                || Preference.LIGHT_THEME_ACCENT_COLOR.key.equals(key)) {
            Style.showRestartDialog(getActivity());
        }
    }

    private void reloadLeftContainer() {
        if (getActivity().findViewById(R.id.edit) != null
                || getActivity().findViewById(R.id.previewContent) != null) {
            Fragment frag = getFragmentManager().findFragmentById(R.id.leftContainer);
            getFragmentManager().beginTransaction().detach(frag).attach(frag).commit();
            getActivity().findViewById(R.id.fab).setVisibility(View.GONE);
        }
    }
}

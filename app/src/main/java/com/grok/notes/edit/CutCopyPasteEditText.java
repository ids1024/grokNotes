package com.grok.notes.edit;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Original:
 * An EditText, which notifies when something was cut/copied/pasted inside it.
 *
 * @author Lukas Knuth
 * @author Guillermo Muntaner on 14/01/16.
 *         <p/>
 *         Source & discussion:
 *         https://stackoverflow.com/questions/14980227/android-intercept-paste-copy-cut-on-edittext
 * @version 1.0
 *          <p/>
 *          Update:
 *          Added a OnCutCopyPasteListener so this class can be used as a plug&play component
 */
public class CutCopyPasteEditText extends EditText {
    public static class Listener {
        public boolean onCut() {
            return false;
        }

        public boolean onCopy() {
            return false;
        }

        public boolean onPaste(String contentsToPaste) {
            return false;
        }
    }

    private Listener listener;

    public void setOnCutCopyPasteListener(Listener listener) {
        this.listener = listener;
    }

    public CutCopyPasteEditText(Context context) {
        super(context);
    }

    public CutCopyPasteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CutCopyPasteEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * <p>This is where the "magic" happens.</p>
     * <p>The menu used to cut/copy/paste is a normal ContextMenu, which allows us to
     * overwrite the consuming method and react on the different events.</p>
     *
     * @see <a href="http://grepcode.com/file/repository.grepcode.com/java/ext/com.google.android/android/2.3_r1/android/widget/TextView.java#TextView.onTextContextMenuItem%28int%29">Original Implementation</a>
     */
    @Override
    public boolean onTextContextMenuItem(int id) {
        boolean consumed = false;
        switch (id) {
            case android.R.id.cut:
                consumed = onCut();
                break;
            case android.R.id.copy:
                consumed = onCopy();
                break;
            case android.R.id.paste:
                consumed = onPaste();
                break;
        }
        return consumed ? consumed : super.onTextContextMenuItem(id);
    }

    public boolean onCut() {
        return listener != null ? listener.onCut() : false;
    }

    public boolean onCopy() {
        return listener != null ? listener.onCopy() : false;
    }

    public boolean onPaste() {
        if (listener != null) {
            ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            CharSequence pastedText = item.getText();
            if (pastedText != null) {
                return listener.onPaste(pastedText.toString());
            }
        }
        return false;
    }
}

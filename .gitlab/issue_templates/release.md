Cut a release for version ____

* [ ] Update the `build.gradle` with the versions
    * [ ] version name
    * [ ] version code
* [ ] Update the `readme` with the new changelog
* [ ] Screenshots
* [ ] Update the website with new features and screenshots
* [ ] Build the signed APK
* [ ] Verify the signed APK works (`adb install app/app-release.apk`)
* [ ] Commit version and readme updates
* [ ] Cut a git tag (`git tag v1.2.3`)
* [ ] Push everything (`git push && git push --tags`)
* [ ] Edit the release notes for the tag (gitlab > repository > tags)
    * [ ] Upload apk and rename it to `grokNotes-<version>.apk`
    * [ ] Add summary and changelog (see below for template)
* [ ] Upload APK and add to "What's new" section on android dev console
* [ ] Close out milestone
* [ ] Create new milestone

#### Tag release notes

```
##### [grokNotes-.apk]()

summary 

###### Changes

- changelog

```